# Contributor: Michael Mason <ms13sp@gmail.com>
# Maintainer: Natanael Copa <ncopa@alpinelinux.org>
pkgname=nano
pkgver=4.9.1
pkgrel=0
pkgdesc="Enhanced clone of the Pico text editor"
url="https://www.nano-editor.org"
arch="all"
license="GPL-3.0-or-later"
makedepends="file-dev linux-headers ncurses-dev"
subpackages="$pkgname-doc $pkgname-syntax::noarch"
source="https://www.nano-editor.org/dist/v${pkgver%.*.*}/nano-$pkgver.tar.xz"

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--disable-nls
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
	install -Dm644 doc/sample.nanorc \
		"$pkgdir"/etc/nanorc

	# Proper syntax highlighting for APKBUILDs
	sed -i "/syntax/s/\"$/\" \"APKBUILD&/" \
		"$pkgdir"/usr/share/nano/sh.nanorc

	rm -rf "$pkgdir"/usr/lib/charset.alias
}

syntax() {
	pkgdesc="Syntax highlighting definitions for $pkgname"

	mkdir -p "$subpkgdir"/usr/share/$pkgname/
	mv "$pkgdir"/usr/share/$pkgname/*.nanorc \
		"$subpkgdir"/usr/share/$pkgname/
}

sha512sums="9e4265424f08afc614f4c6c5f994173ed9639f065b7e81808872c53264cbab582cd6071704e17c3d706d9048df07f3b0733fd688879576cffb8f9897ff4b8505  nano-4.9.1.tar.xz"
